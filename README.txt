Jcss Module :
=============
This module purpose is to help RTL users to do some of the dirty work in RTLing a css
the module will accept a css content and will do the following replacments

* margin-right <==> margin-left
* padding-right <==> padding-left
* float : swap right and left
* text-align : swap right and left,
* all left to right swap i.e. left, margin-left, padding-left, border-left
* backgound or backgroung-position : will swap (only) left and right (see todo)

the module will leave the original line as a comment so you can see the changes
i.e. margin : 1 2 3 4; ==> margin: 1 4 3 2; /* Jcss.Old margin: 1 2 3 4*/

RTL themers todo
===================
After module run, manually do

* some of the images need to be flipped (save with the same name with rtl addition i.e. corner-rtl.gif)
* add "direction:rtl" to body
* backgound or backgroung-position : check the X parameter and swap 0 or 1 (% or px) with 100%

so :
background: transparent url(image.gif) no-repeat 1px .35em;
will become :
background: transparent url(image.gif) no-repeat 100% .35em;

did you used the module to RTL themes ?
please consider uploading the theme to drupal site so everyone can use it

hopfully there will be more RTL themes

Avior
http://dev-art.net